import com.sun.mail.iap.ConnectionException;

import javax.mail.*;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by Sean Ackerley on 0008, 08-05-17.
 */
class ConnectionHandler {
    private final String propFileName = "credentials/EmailConfig.properties";
    private Session session;
    private Store store;
    private Folder fromFolder;
    private Folder toFolder;
    private Message message[];
    private Properties sessionProperties;
    private PropertiesContainer emailData = new PropertiesContainer(propFileName);

    public ConnectionHandler() throws ConnectionException, MessagingException, IOException {
            sessionProperties = new Properties();
            session = Session.getInstance(sessionProperties);
            store = session.getStore("imaps");

            store.connect(emailData.getVar("host"), emailData.getVar("user"), emailData.getVar("password"));

            fromFolder = store.getFolder("Emerch");
            fromFolder.open(Folder.READ_ONLY);
            toFolder = store.getFolder("Conveyor");
            message = fromFolder.getMessages();
    }

    public Message[] getMessage()
    {
        return this.message;
    }

    public Folder getFromFolder()
    {
        return this.fromFolder;
    }

    public Folder getToFolder()
    {
        return this.toFolder;
    }
}
