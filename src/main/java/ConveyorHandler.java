import com.sun.mail.iap.ConnectionException;

import javax.mail.*;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Sean Ackerley on 0003, 03-11-16.
 */
public class ConveyorHandler {

    private ConnectionHandler connection;
    private int lastCount;
    private static String filePath = "src/main/resources/util/LastConveyorCount.txt";

    public ConveyorHandler() throws MessagingException, IOException, ConnectionException    {
        this.connection = new ConnectionHandler();
        this.lastCount = GetLastConveyorCount();
    }

    public void BeginConveyor() throws MessagingException, IOException{

        if (connection.getMessage().length > GetLastConveyorCount())    {
            ConveyMessages();
        }
        if (connection.getMessage().length < GetLastConveyorCount())    {
            CorrectConveyor();
            ConveyMessages();
        }
    }

    private void CorrectConveyor() throws IOException    {
        UpdateLastConveyorCount(connection.getMessage().length - (GetLastConveyorCount() - connection.getMessage().length));
    }

    private void ConveyMessages() throws MessagingException, IOException {

        Message[] forwardList;

        int index = lastCount;

        for (int i = index; i < connection.getMessage().length; i++) {
            if (connection.getMessage()[i].getSubject().compareTo("NAB Transact - eMerch export file") == 0) {
                forwardList = connection.getFromFolder().getMessages(i + 1, i + 1);
                connection.getFromFolder().copyMessages(forwardList, connection.getToFolder());
            }

            if (connection.getMessage()[i].getSubject().compareTo("NAB Transact : eMerch Signup Parser : SUCCESS") == 0 ||
                    connection.getMessage()[i].getSubject().compareTo("NAB Transact : eMerch Signup Parser : FAILURE") == 0) {
                forwardList = connection.getFromFolder().getMessages(i + 1, i + 1);
                connection.getFromFolder().copyMessages(forwardList, connection.getToFolder());

            }
        }

        UpdateLastConveyorCount(connection.getMessage().length);
    }

    private static int GetLastConveyorCount() throws IOException {
        int lastConveyorCount;
        String content = new String(Files.readAllBytes(Paths.get(filePath)));
        lastConveyorCount = Integer.parseInt(content.replace("\n", "").replace("\r", ""));

        return lastConveyorCount;
    }

    private void UpdateLastConveyorCount(int count) throws IOException   {
        List<String> lines = Arrays.asList(Integer.toString(count));
        Path file = Paths.get(filePath);

        Files.write(file, lines, Charset.forName("UTF-8"));

    }

}
