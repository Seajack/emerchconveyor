import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Sean Ackerley on 0010, 10-05-17.
 */
public class Main {

    public static void main(String[] args) {
//        while(true) {
            try {
                ConveyorHandler convey = new ConveyorHandler();
                convey.BeginConveyor();
            } catch(Exception e)    {
                try {
                    LogException(e);
                } catch(IOException ie)    {
                    ie.printStackTrace();
                    System.exit(1);
                }
            }
            System.exit(0);
//        }
    }

    private static void LogException(Exception e) throws IOException   {
        FileWriter fWriter = new FileWriter ("src/main/resources/util/conveyorlog.txt", true);
        PrintWriter pWriter = new PrintWriter (fWriter);
        e.printStackTrace (pWriter);
        pWriter.close();
        fWriter.close();
    }
}
